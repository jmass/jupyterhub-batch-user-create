use rand::prelude::*;
use rand_chacha::ChaCha20Rng;

pub const DEFAULT_CHARSET: &str = "ABCDEFGHKLMNPQRTUVWXYZabcdefghkmnpqrtuvwxyz123456789*&#@.";

pub fn create_password(chars: &str, length: usize) -> String {
    let mut rng = ChaCha20Rng::from_entropy();
    let password: String = (0..length)
        .map(|_| {
            let idx = rng.gen_range(0..chars.len());
            let temp: Vec<char> = chars.chars().collect();
            match temp.get(idx) {
                Some(c) => *c,
                None => '_',
            }
        })
        .collect();
    password
}
