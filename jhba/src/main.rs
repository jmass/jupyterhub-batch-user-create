use clap::Parser;
use jhba::{create_password, DEFAULT_CHARSET};
use std::fmt::{Display, Formatter};

#[derive(Debug, serde::Deserialize, serde::Serialize)]
struct Token {
    token: String,
}

#[derive(Debug)]
struct User {
    name: String,
    password: String,
    token: Token,
}

impl Display for User {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}\t{}\t{}", self.name, self.password, self.token.token)
    }
}
#[derive(Parser, Debug)]
#[clap(author, version, about, long_about = None)]
struct Args {
    /// Username to create passwords for
    #[clap(short, long)]
    username: String,

    /// Length of the passwords. Values shorter than 7 characters are not supported.
    #[clap(short, long, default_value_t = 8)]
    password_length: u8,

    /// Admin api token
    #[clap(short, long)]
    admin_api_token: String,

    /// JupyterHub server address, e.g. http://localhost:8899
    #[clap(short, long)]
    base_url: String,

    /// Show generated token for user in output
    #[clap(short, long, takes_value = false)]
    show_token: bool,

    /// Create user if not exists
    #[clap(short, long, takes_value = false)]
    create_user: bool,

    /// Show more info
    #[clap(short, long, takes_value = false)]
    verbose: bool,
}

fn main() -> Result<(), reqwest::Error> {
    let args = Args::parse();

    let client = reqwest::blocking::Client::new();
    let jh_url = args.base_url;
    let username = args.username;
    let admin_api_token = args.admin_api_token;
    let url_create_user = format!("{}{}{}", jh_url, "/hub/api/users/", username);
    let url_create_token = format!("{}{}{}{}", jh_url, "/hub/api/users/", username, "/tokens");
    let url_set_password = format!("{}{}", jh_url, "/hub/auth/change-password");

    if args.password_length < 7 {
        eprintln!("Error: Passwords must be at least 7 characters long.");
        std::process::exit(1)
    }
    let new_password = create_password(DEFAULT_CHARSET, args.password_length as usize);

    let mut user = User {
        name: username,
        password: new_password,
        token: Token {
            token: "".to_string(),
        },
    };

    // create new user if -c / --create-user  option is set
    if args.create_user {
        let res_create_user = client
            .post(url_create_user)
            .body("")
            .bearer_auth(admin_api_token.clone()) //todo
            .send();
        match res_create_user {
            Ok(response) => {
                eprintln!("INFO: {}", response.text().unwrap());
            }
            Err(e) => {
                eprintln!("ERROR: Error creating user.\n{}", e);
                std::process::exit(1);
            }
        }
    }

    // create api token for user so that password can be (re-)set
    let res_create_token = client
        .post(url_create_token)
        .body("")
        .bearer_auth(admin_api_token)
        .send();

    match res_create_token {
        Ok(response) => {
            if let Ok(token) = response.json::<Token>() {
                user.token = token;
            } else {
                if !args.create_user {
                    eprintln!("ERROR: Error creating token for user {}.\nRun with --create-user if the user doesn't exist yet.", user.name)
                } else {
                    eprintln!(
                        "ERROR: Error creating token for user {}.\nIs your admin api token valid?",
                        user.name
                    );
                }
                std::process::exit(1)
            }
            if args.show_token {
                println!("{}", user);
            } else {
                println!("{}\t{}", user.name, user.password);
            }
        }
        Err(e) => {
            eprintln!("ERROR: Error creating token.\n{}", e);
            std::process::exit(1);
        }
    }

    let params = [("password", user.password)];
    let result_change_pw = client
        .post(url_set_password)
        .form(&params)
        .bearer_auth(user.token.token)
        .send();

    match result_change_pw {
        Ok(response) => {
            let result: String = response.text().unwrap();
            // reset.html from firstuseauthenticator
            if result.contains("alert-danger") {
                eprintln!("ERROR: Something went wrong while setting the new password.");
                if args.verbose {
                    eprintln!("INFO:\n {}", result);
                }
                std::process::exit(1)
            }
        }
        Err(e) => {
            eprintln!(
                "ERROR: Error setting password for user {}.\n{}",
                user.name, e
            );
            std::process::exit(1);
        }
    }

    Ok(())
}
