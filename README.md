# Jupyterhub Batch User Create

command line utility to create users and set passwords on jupyterhub 


## Prerequisites 
As admin user, obtain an API token, e.g. from  `` https://<SERVER>/hub/token `` -> Click on "Request new API token". 


## Examples

```
 ./jhba --help
jhba 0.1.0


USAGE:
    jhba [OPTIONS] --username <USERNAME> --admin-api-token <ADMIN_API_TOKEN> --base-url <BASE_URL>

OPTIONS:
    -a, --admin-api-token <ADMIN_API_TOKEN>    Admin api token
    -b, --base-url <BASE_URL>                  JupyterHub server address, e.g. http://localhost:8899
    -c, --create-user                          Create user if not exists
    -h, --help                                 Print help information
    -p, --password-length <PASSWORD_LENGTH>    Length of the passwords [default: 8]
    -s, --show-token                           Show generated token for user in output
    -u, --username <USERNAME>                  Username to create passwords for
    -V, --version                              Print version information

```


Create a single user with a randomly generated password of length 16:

```
./jhba -c -p 16 -u myfirstuser -b http://localhost:8899  -a 2ef024XXXXXXXXXXXXXXXXXXXX6753537
```



Create a bunch of users:

```
for i in {0..2}; 
	do for j in {0..9}; 
		do ./jhba --admin-api-token 2ef024XXXXXXXXXXXXXXXXXXXX6753537 --base-url http://localhost:8899  --create-user --username testuser$i$j >>out;
	done;
done
```

